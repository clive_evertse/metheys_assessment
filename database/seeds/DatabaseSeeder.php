<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
            factory(App\Owner::class, 10)->create()->each(function($u) {
        $u->vehicle()->save(factory(App\Vehicle::class)->make());
    });
    }
}
