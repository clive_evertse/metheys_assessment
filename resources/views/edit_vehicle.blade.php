@extends('layouts.app')

@section('content')

@if (count($errors) > 0)
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
               <ul>
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-warning" role="alert">{{$error}}</div>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif
<div class="container">
    <div class="row">
          <div class="form-group">
              <div class="col-sm-offset-1 col-sm-10 ">      
                  <a href="{{url('profile')}}"><button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span>Back</button></a>      
              </div>
        </div>
        <div class="col-md-10 col-md-offset-1">
            <h2 id=tables-bordered>Edit Vehicle and Owner</h2>
            <form class="form-horizontal" action="{{url('update_owner')}}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="owner_id" value="{{$owner->id}}">
                <div class="form-group">
                    <label for="ownersFirstName" class="col-sm-2 control-label">Owners First Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ownersFirstName" placeholder="Owners First Name" name="first_name" value="{{$owner->first_name}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="ownersLastName" class="col-sm-2 control-label">Owners Last Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ownersLastName" placeholder="Owners Last Name" name="last_name" value="{{$owner->last_name}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="contactNumber" class="col-sm-2 control-label">Contact Number</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="contactNumber" placeholder="Contact Number" name="contact_number" value="{{$owner->contact_number}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email" value="{{$owner->email}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="milage" class="col-sm-2 control-label">Manufacturer</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="manufacturer" placeholder="Manufacturer" name="manufacturer" value="{{$owner->vehicle->manufacturer}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="type" class="col-sm-2 control-label">Type (Model)</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="type" placeholder="Type (Model)" name="type" value="{{$owner->vehicle->type}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="year" class="col-sm-2 control-label">Year</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="year" placeholder="Year" name="year" value="{{$owner->vehicle->year}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="colour" class="col-sm-2 control-label">Colour</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="colour" placeholder="Colour" name="colour" value="{{$owner->vehicle->colour}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="ownersLastName" class="col-sm-2 control-label">Mileage</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="mileage" placeholder="Mileage" name="mileage" value="{{$owner->vehicle->mileage}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Save</button>
                    </div>
                </div>
        </form>
      </div>
    </div>
</div>
@endsection