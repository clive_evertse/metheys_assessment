@extends('layouts.app')

@section('content')

@if (isset($flag))
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
               <ul>
                        @if($flag == 'newOwner')
                        <div class="alert alert-success" role="alert">Vehicle and Owner Successfully Created</div>
                        @elseif($flag == 'updatedSuccess')
                        <div class="alert alert-success" role="alert">Vehicle and Owner Successfully Edited</div>
                        @elseif($flag == 'deleted')
                        <div class="alert alert-success" role="alert">Vehicle and Owner Successfully Deleted</div>
                        @endif
                </ul>
            </div>
        </div>
    </div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
                <h2 id=tables-bordered>Owned Vehicle List</h2> 
                <table class="table table-bordered"> 
                    <thead> 
                        <tr>                    
                            <th>First Name</th> 
                            <th>Vehicle manufacturer & type</th> 
                            <th>Date registered</th>
                            <th>View Item</th>
                            <th>Edit Item</th>
                            <th>Delete Item</th>  
                        </tr> 
                    </thead> 
                    <tbody>
                    @foreach($owners as $owner) 
                        <tr>                    
                            <td>{{ $owner->first_name }}</td> 
                            <td>{{ $owner->vehicle->manufacturer }}</td> 
                            <td>{{ date('d F, Y', strtotime($owner->vehicle->created_at))}}</td>
                            <td><a href="{{url('view_item')}}/{{$owner->id}}"><span class="glyphicon glyphicon-eye-open col-md-offset-5" aria-hidden=true></span></a></td>
                            <td><a href="{{url('edit_item')}}/{{$owner->id}}"><span class="glyphicon glyphicon-pencil col-md-offset-5" aria-hidden=true></span></a></td>
                            <td><a href="{{url('confirm_delete')}}/{{$owner->id}}"><span class="glyphicon glyphicon-remove col-md-offset-5" aria-hidden=true></span></a></td>                     
                        </tr>
                    @endforeach
                    </tbody> 
                </table>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="btn-group btn-group-justified" role=group aria-label="Justified button group"> 
                <div class=btn-group role=group> <a href="{{url('create_vehicle')}}"><button type=button class="btn btn-default">Add New Vehicle</button></a> </div>
            </div>
        </div>
    </div>
</div> 
@endsection