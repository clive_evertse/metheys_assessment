<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Http\Request;



Route::group(['middleware' => 'web'],function(){
	Route::auth();

	Route::get('/', function () {
    	return view('welcome');
	});

	Route::get('/home', 'HomeController@index');
	Route::get('profile','HomeController@index');
	Route::get('profile/success','HomeController@newOwnerSuccess');
	Route::get('profile/edited','HomeController@editedOwnerSuccess');
	Route::get('profile/deleted','HomeController@deletedOwnerSuccess');

	Route::get('view_item/{id}','OwnerController@viewRecord');	
	Route::get('create_vehicle','OwnerController@createVehicle');
	Route::post('add_vehicle', 'OwnerController@createRecord');
	Route::get('edit_item/{id}','OwnerController@editItem');
	Route::post('update_owner','OwnerController@updateRecord');
	Route::get('confirm_delete/{id}','OwnerController@confirmDelete');
	Route::get('delete_item/{id}','OwnerController@deleteRecord');

	
});

