<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Illuminate\Validation\ValidationException;
use App\Http\Requests;
use App\Vehicle as Vehicle;
use App\Owner as Owner;
use Validator;
use Illuminate\Support\Facades\Input;

class OwnerController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewRecord($id){
    	$owner = Owner::find($id);
    	return view('view_owner', array('owner' => $owner));
    }

    public function createVehicle(){
    	return view('add_vehicle');
    }

    public function createRecord(Request $request)
    {
     	  
		$rules = array(
            'first_name' => 'required|string|max:45',
            'last_name' =>  'required|string|max:45',
            'email' => 'required|email',
            'contact_number' => 'required|string|max:11',
            'manufacturer' => 'required|string|max:45',
            'type' => 'required|string|max:45',
            'year' => 'required|string|max:4',
            'colour' => 'required|string|max:11',
            'mileage' => 'required|string|max:15'
        );
        
		$input = Input::all();
		$validation = Validator::make($input,$rules);

		if($validation->fails()){

			$messages = $validation->messages();
			return redirect('create_vehicle')->withErrors($validation)->withInput();
		}
		else{

				$owner = new Owner;
				$owner->first_name = Input::get('first_name');
				$owner->last_name = Input::get('last_name');
				$owner->contact_number = Input::get('contact_number');
				$owner->email = Input::get('email');

				$owner->save();

				$vehicle = new Vehicle;
				$vehicle->owner_id = $owner->id;
				$vehicle->manufacturer = Input::get('manufacturer');
				$vehicle->type = Input::get('type');
				$vehicle->year = Input::get('year');
				$vehicle->colour = Input::get('colour');
				$vehicle->mileage = Input::get('mileage');

				$vehicle->save();

				return redirect('profile/success');
		}
		
    }
    public function editItem($id){
    	$owner = Owner::find($id);
		return view('edit_vehicle',array('owner' => $owner));	
    }

    public function updateRecord(){

        //Updates the Owner and Vehicle
		$input = Input::all();
		$id = Input::get('owner_id');
		$owner =  Owner::find($id)->first();
		$owner->first_name = Input::get('first_name');
		$owner->last_name = Input::get('last_name');
		$owner->contact_number = Input::get('contact_number');
		$owner->email = Input::get('email');

		$owner->save();

		$vehicle = Vehicle::where('owner_id',$id)->first();
		$vehicle->manufacturer = Input::get('manufacturer');
		$vehicle->type = Input::get('type');
		$vehicle->year = Input::get('year');
		$vehicle->colour = Input::get('colour');
		$vehicle->mileage = Input::get('mileage');

		$vehicle->save();

		return redirect('profile/edited');
    }

    public function confirmDelete($id){
    	$owner = Owner::find($id);
    	return view('confirm_delete',array('owner' => $owner));
    }

    public function deleteRecord($id){
    	//Soft Delete
    	$owner = Owner::find($id);
    	$owner->delete();

    	//Cascade not working?
    	$vehicle = Vehicle::where('owner_id',$id);
    	$vehicle->delete();

    	return redirect('profile/deleted');
    }
}
