<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Owner as Owner;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $owners = Owner::all();
        return view('profile',array('owners' => $owners));
    }

    public function newOwnerSuccess(){
        $owners = Owner::all();
        return view('profile',array('owners' => $owners, 'flag' => 'newOwner'));
    }

    public function editedOwnerSuccess(){
        $owners = Owner::all();
        return view('profile',array('owners' => $owners, 'flag' => 'updatedSuccess'));      
    }

    public function deletedOwnerSuccess(){
         $owners = Owner::all();
        return view('profile',array('owners' => $owners, 'flag' => 'deleted'));        
    }
}
