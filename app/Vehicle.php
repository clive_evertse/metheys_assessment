<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    //
    public function owner(){
    	return $this->belongsTo('App\Owner');
    }
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
