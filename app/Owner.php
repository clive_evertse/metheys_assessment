<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends Model
{
    //
    public function vehicle(){
	    return $this->hasOne('App\Vehicle');  	
    }

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
